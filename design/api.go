// 江月科技 http://www.moonline.cc
// 作者：武羡峰 painkiller0513@163.com
// 日期：2018-04-06
// 说明：线路微服务接口
// 测试自动构建

package design

import (
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = API("activity", func() {
	Title("活动微服务")
	Description("活动是根据线路创建出来的，用于报名。如果是测试环境，api secret 请使用30585f550035f591398a85b974007601")
	Version("1.0.0")
	Consumes("application/json")
	Produces("application/json")

	// 服务器信息
	Scheme("http")
	Host("localhost:8080")
	BasePath("/activity")

	// 联系信息
	Contact(func() {
		Name("江月科技")
		Email("xianfeng@moonline.cc")
		URL("http://www.moonline.cc")
	})

	// CORS跨域请求设置
	Origin("*", func() {
		Headers("Content-Type", "X-Shared-Secret")
		Methods("GET", "POST", "PUT", "DELETE")
		Expose("X-Time")
		MaxAge(300)
		Credentials()
	})
})

// API授权
var APIKey = APIKeySecurity("api_key", func() {
	Header("X-Shared-Secret")
	Description("《路友演示小程序》ks9ljuBGfcRVWfLNd1tdvBQZuOGecZ5hhCLbjrkiYusv4eI3")
})

// 定义一些常用的常量
const (
	// 字符串
	// 图片url
	IMAGE_URL = "http://img.luyou77.com/img.png"
	// 网页url
	PAGE_URL = "https://36kr.com/p/5135395.html"

	// 整型
	// url最小长度
	URL_MINLENGTH = 7
	// url最大长度
	URL_MAXLENGTH  = 255
	HTML_MAXLENGTH = 1024 * 10
)
